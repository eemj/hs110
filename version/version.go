package version

import (
	"errors"
	"net"
)

const (
	Version104Port = "9999"
	Version110Port = "80"
)

type Version string

const (
	Version104 = "v1.0.4"
	Version110 = "v1.1.0"
)

var (
	ErrUnableToDetermineVersion = errors.New("unable to determine version")
)

// DetermineVersion will determine the client's version by attempting to establish a TCP
// connection with them. Every version that we have a defined protocol uses a different port.
func DetermineVersion(host string) (v Version, err error) {
	conn, err := net.Dial("tcp", net.JoinHostPort(host, Version104Port))

	if err == nil {
		v = Version104
		err = conn.Close()
		return
	}

	conn, err = net.Dial("tcp", net.JoinHostPort(host, Version110Port))

	if err == nil {
		v = Version110
		err = conn.Close()
		return
	}

	err = ErrUnableToDetermineVersion

	return
}
