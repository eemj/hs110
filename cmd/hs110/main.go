package main

import (
	"log"

	"gitlab.com/eemj/hs110/api"
	"gitlab.com/eemj/hs110/client/v1.0.4"
)

func main() {
	c := client.NewClient(&client.ClientOptions{
		Host: "172.16.0.4",
	})

	info, err := api.SysInfo(c)

	if err != nil {
		log.Fatal(err)
	}

	log.Print(info.System.GetSysInfo.Alias)

	switch info.System.GetSysInfo.RelayState {
	case 1:
		log.Print("device is currently on")
	case 0:
		log.Print("device is currently off")
	}
}
