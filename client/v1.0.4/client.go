package client

import (
	"io"
	"net"
	"time"
)

// Client defines the client for the Kasa devices with 'v1.0.4' firmware version.
type Client struct {
	conn net.Conn
	addr string
}

// ClientOptions defines the options required to connect to host
type ClientOptions struct {
	Host string
}

func (c *Client) Addr() string { return c.addr }

func NewClient(options *ClientOptions) (client *Client) {
	client = &Client{
		addr: net.JoinHostPort(options.Host, "9999"),
	}

	return
}

func (c *Client) Request(body []byte) (buf []byte, err error) {
	conn, err := net.Dial("tcp", c.Addr())

	if err != nil {
		return
	}

	conn.SetWriteDeadline(time.Now().Add(time.Second))

	_, err = conn.Write(packInt(int32(len(body))))

	if err != nil {
		return
	}

	_, err = conn.Write(encrypt(body))

	if err != nil {
		return
	}

	conn.SetReadDeadline(time.Now().Add(time.Second))

	var (
		tmpBuf = make([]byte, 1024)
		read   = -1
		length = 0
	)

	for read < length {
		n, err := conn.Read(tmpBuf)

		if err != nil {
			if err != io.EOF {
				return nil, err
			}

			break
		}

		read += n

		if length == 0 {
			length = int(unpackInt(tmpBuf[:4]))
			tmpBuf = tmpBuf[4:]
			n -= 4
		}

		buf = append(buf, tmpBuf[:n]...)
	}

	if err = conn.Close(); err != nil {
		return
	}

	buf = decrypt(buf)

	return
}
