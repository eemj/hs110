package client

// Sourced from: https://github.com/fffonion/tplink-plug-exporter/blob/master/kasa/kasa.go

const key = 171

func packInt(in int32) []byte {
	out := make([]byte, 4)
	for i := 3; i > 0; i-- {
		out[i] = byte(in & 0xFF)
		in >>= 8
	}
	return out
}

func unpackInt(in []byte) int32 {
	length := 0
	for i := 0; i < 4; i++ {
		length <<= 8
		length += int(in[i])
	}
	return int32(length)
}

func encrypt(in []byte) []byte {
	length := len(in)
	out := make([]byte, length)

	key := key
	for i, r := range in {
		key = key ^ int(r)
		out[i] = byte(key)
	}
	return out
}

func decrypt(in []byte) []byte {
	length := len(in)
	out := make([]byte, length)
	key := key
	for i := 0; i < length; i++ {
		b := int(in[i])
		out[i] = byte(key ^ b)
		key = b
	}
	return out
}

