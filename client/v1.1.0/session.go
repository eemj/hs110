package client

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/binary"
	"sync/atomic"
)

type IV struct {
	Data     []byte
	Sequence uint32
}

func NewIV(b []byte) (iv *IV, err error) {
	reader := bytes.NewReader(b[12:16])

	var num uint32

	if err = binary.Read(reader, binary.BigEndian, &num); err != nil {
		return
	}

	iv = &IV{
		Data:     b[:12],
		Sequence: num & 0x7fffffff,
	}

	return
}

type Session struct {
	iv        IV
	key       []byte
	signature []byte
}

func NewSession(
	localSeed, remoteSeed, userHash []byte,
) (session *Session, err error) {
	session = &Session{}

	payload := append([]byte("lsk"), append(
		localSeed, append(
			remoteSeed, userHash...,
		)...,
	)...)

	hasher := sha256.New()

	if _, err = hasher.Write(payload); err != nil {
		return
	}

	session.key = hasher.Sum(nil)[:16]

	payload = append([]byte("ldk"), append(
		localSeed, append(
			remoteSeed,
			userHash...,
		)...,
	)...)

	hasher.Reset()

	if _, err = hasher.Write(payload); err != nil {
		return
	}

	session.signature = hasher.Sum(nil)[:28]

	payload = append([]byte("iv"), append(
		localSeed, append(
			remoteSeed,
			userHash...,
		)...,
	)...)

	hasher.Reset()

	if _, err = hasher.Write(payload); err != nil {
		return
	}

	iv, err := NewIV(hasher.Sum(nil))

	if err != nil {
		return
	}

	session.iv = *iv

	return
}

func (s *Session) GetSequence() uint32 {
	return atomic.LoadUint32(&s.iv.Sequence)
}

func (s *Session) GetIV() []byte {
	buf := make([]byte, 4)
	binary.BigEndian.PutUint32(buf, s.iv.Sequence)
	return append(s.iv.Data, buf...)
}

func (Session) pkcs5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func (Session) pkcs5Trimming(encrypt []byte) []byte {
	padding := encrypt[len(encrypt)-1]
	return encrypt[:len(encrypt)-int(padding)]
}

func (s *Session) Encrypt(payload []byte) (encrypted []byte, err error) {
	atomic.AddUint32(&s.iv.Sequence, 1)

	block, err := aes.NewCipher(s.key)

	if err != nil {
		return
	}

	ecb := cipher.NewCBCEncrypter(block, s.GetIV())

	payload = s.pkcs5Padding(payload, block.BlockSize())
	crypted := make([]byte, len(payload))
	ecb.CryptBlocks(crypted, payload)

	hasher := sha256.New()

	buf := make([]byte, 4)
	binary.BigEndian.PutUint32(buf, s.iv.Sequence)

	msg := append(
		s.signature,
		append(
			buf,
			crypted...,
		)...,
	)

	_, err = hasher.Write(msg)

	if err != nil {
		return
	}

	encrypted = append(
		hasher.Sum(nil),
		crypted...,
	)

	return
}

func (s *Session) Decrypt(payload []byte) (decrypted []byte, err error) {
	block, err := aes.NewCipher(s.key)

	if err != nil {
		return
	}

	payload = payload[32:]

	ecb := cipher.NewCBCDecrypter(block, s.GetIV())
	decrypted = make([]byte, len(payload))
	ecb.CryptBlocks(decrypted, payload)

	decrypted = s.pkcs5Trimming(decrypted)

	return
}
