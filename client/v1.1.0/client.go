package client

import (
	"bytes"
	"crypto/md5"
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/cookiejar"
)

var (
	ErrStatusCodeNot200 = errors.New("status code != 200")
	ErrChecksumFailed   = errors.New("checksum failed")
)

const (
	contentTypeHTML  = "text/html"
	contentTypePlain = "text/plain"
)

type Client struct {
	httpClient *http.Client

	options *ClientOptions

	localSeed  []byte
	remoteSeed []byte
	session    *Session
}

type ClientOptions struct {
	Host     string
	Username string
	Password string
}

func NewClient(options *ClientOptions) (client *Client, err error) {
	jar, err := cookiejar.New(nil)

	if err != nil {
		return
	}

	seed := make([]byte, 16)

	n, err := rand.Read(seed)

	if err != nil {
		return
	}

	client = &Client{
		httpClient: &http.Client{Jar: jar},
		options:    options,

		localSeed: seed[:n],
	}

	for {
		if err = client.Handshake(); err != nil {
			return
		}

		if _, err = client.Request([]byte(
			`{"time":{"get_time":null}}`,
		)); err == nil {
			break
		}
	}

	return
}

func (c *Client) Addr() string {
	return net.JoinHostPort(c.options.Host, "80")
}

func (c *Client) GetLocalSeed() []byte { return c.localSeed }

func (c *Client) GetRemoteSeed() []byte { return c.remoteSeed }

func (c *Client) GetSession() *Session { return c.session }

func (c *Client) UserHash() (hash []byte, err error) {
	concat := make([]byte, 0)

	hasher := md5.New()

	_, err = hasher.Write([]byte(c.options.Username))

	if err != nil {
		return
	}

	concat = append(concat, hasher.Sum(nil)...)

	hasher.Reset()

	_, err = hasher.Write([]byte(c.options.Password))

	if err != nil {
		return
	}

	concat = append(concat, hasher.Sum(nil)...)

	hasher.Reset()

	_, err = hasher.Write(concat)

	if err != nil {
		return
	}

	hash = hasher.Sum(nil)

	return
}

func (c *Client) Request(body []byte) (data []byte, err error) {
	body, err = c.session.Encrypt(body)

	if err != nil {
		return
	}

	data, _, err = c.Post(
		fmt.Sprintf(
			"http://%s/app/request?seq=%d",
			c.Addr(), c.session.GetSequence(),
		),
		contentTypePlain,
		bytes.NewReader(body),
	)

	if err != nil {
		return
	}

	data, err = c.session.Decrypt(data)

	return
}

func (c *Client) Post(url, contentType string, reader io.Reader) (content []byte, statusCode int, err error) {
	res, err := c.httpClient.Post(url, contentType, reader)

	if err != nil {
		return
	}

	statusCode = res.StatusCode

	if statusCode != http.StatusOK {
		err = ErrStatusCodeNot200
		return
	}

	content, err = ioutil.ReadAll(res.Body)

	if err != nil {
		return
	}

	defer res.Body.Close()

	return
}

func (c *Client) Handshake() (err error) {
	userHash, err := c.UserHash()

	if err != nil {
		return
	}

	content, _, err := c.Post(
		fmt.Sprintf("http://%s/app/handshake1", c.Addr()),
		contentTypePlain,
		bytes.NewReader(c.localSeed),
	)

	if err != nil {
		return
	}

	hasher := sha256.New()

	checksum := append(c.localSeed, userHash...)

	_, err = hasher.Write(checksum)

	if err != nil {
		return
	}

	checksum = hasher.Sum(nil)

	if bytes.Compare(content[16:], checksum) != 0 {
		err = ErrChecksumFailed
		return
	}

	hasher.Reset()

	c.remoteSeed = content[:16]

	_, err = hasher.Write(append(c.remoteSeed, userHash...))

	if err != nil {
		return
	}

	content, _, err = c.Post(
		fmt.Sprintf("http://%s/app/handshake2", c.Addr()),
		contentTypePlain,
		bytes.NewReader(hasher.Sum(nil)),
	)

	if err != nil {
		return
	}

	c.session, err = NewSession(
		c.localSeed, c.remoteSeed, userHash,
	)

	return
}
