package hs110

import (
	"gitlab.com/eemj/hs110/client"
	client104 "gitlab.com/eemj/hs110/client/v1.0.4"
	client110 "gitlab.com/eemj/hs110/client/v1.1.0"
	"gitlab.com/eemj/hs110/version"
)

type Options struct {
	Host     string
	Username string
	Password string
}

func New(options Options) (c client.ClientInterface, err error) {
	v, err := version.DetermineVersion(options.Host)

	if err != nil {
		return
	}

	switch v {
	case version.Version104:
		c = client104.NewClient(&client104.ClientOptions{
			Host: options.Host,
		})
	case version.Version110:
		c, err = client110.NewClient(&client110.ClientOptions{
			Host:     options.Host,
			Username: options.Username,
			Password: options.Password,
		})
	}

	return
}
