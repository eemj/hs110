package api

import (
	"encoding/json"

	"gitlab.com/eemj/hs110/client"
)

type EmeterResponse struct {
	Emeter struct {
		GetRealtime struct {
			CurrentMA int64 `json:"current_ma"`
			VoltageMV int64 `json:"voltage_mv"`
			PowerMW   int64 `json:"power_mw"`
			TotalWH   int64 `json:"total_wh"`
			ErrorCode int64 `json:"err_code"`
		} `json:"get_realtime"`
	} `json:"emeter"`
}

type GetVGainIGain struct {
	GetVGainIGain struct{} `json:"get_vgain_igain"`
}

type GetRealtime struct {
	GetRealtime struct{} `json:"get_realtime"`
}

type GetDaystat struct {
	GetDaystat struct {
		Month int `json:"month"`
		Day   int `json:"day"`
	} `json:"get_daystat"`
}

type GetMonthstat struct {
	GetMonthstat struct {
		Year int `json:"year"`
	} `json:"get_monthstat"`
}

type EmeterRequest struct {
	Emeter interface{} `json:"emeter"`
}

func Emeter(client client.ClientInterface, request EmeterRequest) (emeter *EmeterResponse, err error) {
	payload, err := json.Marshal(request)

	if err != nil {
		return
	}

	data, err := client.Request(payload)

	if err != nil {
		return
	}

	emeter = new(EmeterResponse)

	err = json.Unmarshal(data, emeter)

	return
}
