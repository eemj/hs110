package api

import (
	"encoding/json"

	"gitlab.com/eemj/hs110/client"
)

type SysInfoResponse struct {
	System struct {
		GetSysInfo struct {
			SoftwareVersion string `json:"sw_ver"`
			HardwareVersion string `json:"hw_ver"`
			Model           string `json:"model"`
			DeviceID        string `json:"deviceId"`
			OEMID           string `json:"oemId"`
			HardwareID      string `json:"hwId"`
			RSSI            int64  `json:"rssi"`
			Latitude        int64  `json:"latitude_i"`
			Longitude       int64  `json:"longitude_i"`
			Alias           string `json:"alias"`
			Status          string `json:"status"`
			MicType         string `json:"mic_type"`
			Feature         string `json:"feature"`
			MAC             string `json:"mac"`
			Updating        int8   `json:"updating"`
			LedOff          int8   `json:"led_off"`
			RelayState      int8   `json:"relay_state"`
			OnTime          int64  `json:"on_time"`
			IconHash        string `json:"icon_hash"`
			DevName         string `json:"dev_name"`
			ActiveMode      string `json:"active_mode"`
			NextAction      struct {
				Type int8 `json:"type"`
			} `json:"next_action"`
			NTCState int8 `json:"ntc_state"`
			ErrCode  int8 `json:"err_code"`
		} `json:"get_sysinfo"`
	} `json:"system"`
}

type SysInfoRequest struct {
	System struct {
		GetSysInfo *struct{} `json:"get_sysinfo"`
	} `json:"system"`
}

func SysInfo(
	client client.ClientInterface,
) (sysInfo *SysInfoResponse, err error) {
	request, _ := json.Marshal(&SysInfoRequest{})

	data, err := client.Request(request)

	if err != nil {
		return
	}

	sysInfo = new(SysInfoResponse)

	err = json.Unmarshal(data, sysInfo)

	return
}
