package api

import (
	"encoding/json"

	"gitlab.com/eemj/hs110/client"
)

type SystemResponse struct {
	System interface{} `json:"system"`
}

type SetDeviceAlias struct {
	SetDeviceAlias struct {
		Alias string `json:"alias"`
	} `json:"set_device_alias"`
}

type SetRelayState struct {
	SetRelayState struct {
		State int `json:"state"`
	} `json:"set_relay_state"`
}

type SystemRequest struct {
	System interface{} `json:"system"`
}

func System(client client.ClientInterface, request SystemRequest) (system *SystemResponse, err error) {
	payload, err := json.Marshal(request)

	if err != nil {
		return
	}

	data, err := client.Request(payload)

	if err != nil {
		return
	}

	system = new(SystemResponse)

	err = json.Unmarshal(data, system)

	return
}
